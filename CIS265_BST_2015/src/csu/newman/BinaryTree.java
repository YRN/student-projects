package csu.newman;

import java.util.ArrayList;

// Author: Yvonne Newman
// Date: Apr 21, 2015
// Goal: Create a binary tree with methods to show, order, add, delete and find
//-----------------------------------------------------------------------
public class BinaryTree< E extends Comparable<E>> {

    Tnode<E> root;

    public BinaryTree() {
        root = null;
    }

    public BinaryTree(E... values) {
        for (int i = 0; i < values.length; i++) {
            Tnode<E> newNode = this.add(values[i]);
            if (i == 0) {
                root = newNode;
            }
        }
    }

    public Tnode<E> add(E data) {
        Tnode<E> newNode = new Tnode<>(data);
        if (root == null) {
            root = newNode;
        } else {
            Tnode<E> current = root;
            Tnode<E> previous = null;
            char selectedBranch = 'L';
            while (current != null) {
                previous = current;
                if (newNode.getData().compareTo(current.getData()) >= 0) {
                    current = current.getRight();
                    selectedBranch = 'R';
                } else {
                    current = current.getLeft();
                    selectedBranch = 'L';
                }
            }
            if (selectedBranch == 'L') {
                previous.setLeft(newNode);
            } else {
                previous.setRight(newNode);
            }
            newNode.setParent(previous);
        }
        return newNode;
    }// add

    public void rowWise() {
        System.out.println("\nROW-WISE NAVIGATION:\t");

        ArrayList<Tnode<E>> rowList = new ArrayList<>();
        rowList.add(root);
        while (!rowList.isEmpty()) {
            Tnode<E> current = rowList.remove(0);
            System.out.print(current.getData() + " ");
            if (current.getLeft() != null) {
                rowList.add(current.getLeft());
            }
            if (current.getRight() != null) {
                rowList.add(current.getRight());
            }
        }
        System.out.println();
    }//rowWise

    public void preOrder() {
        System.out.println("\nPRE-ORDER:\t");
        preOrder(root);
    }

    private void preOrder(Tnode<E> target) {
        if (target != null) {
            System.out.print(target.getData() + " ");
            preOrder(target.getLeft());
            preOrder(target.getRight());
        }
    }

    public void postOrder() {
        System.out.println("\nPOST-ORDER:\t");
        postOrder(root);
    }

    private void postOrder(Tnode<E> target) {
        if (target != null) {

            postOrder(target.getRight());
            postOrder(target.getLeft());
            System.out.print(target.getData() + " ");
        }
    }

    public void inOrder() {
        System.out.println("\nIN-ORDER:\t");
        inOrder(root);
    }

    private void inOrder(Tnode<E> target) {
        if (target != null) {
            inOrder(target.getLeft());
            System.out.print(target.getData() + " ");
            inOrder(target.getRight());
        }
    }

    public Tnode<E> find(E data) throws NullPointerException {
        Tnode<E> newNode = new Tnode<>(data);
        Tnode<E> current = root;

        if (root == null) {
            return null;
        } else {
            try {
                //investigate tree
                while (current != null && current.getData().compareTo(data) != 0) {
                    if (newNode.getData().compareTo(current.getData()) >= 0) {
                        current = current.getRight();
                    } else {
                        current = current.getLeft();
                    }
                    //after checking the whole tree, if the value wasn't found
                }
            } catch (NullPointerException e) {
                System.out.println("The value " + data
                        + " does not exist in the tree.\n" + e);
                throw e;
            }
        }

        return current;
    } //find

    public Tnode<E> delete(E keyValue) {
        //move down the left side of the tree
        //and bring up the rightmost
        try {

            Tnode<E> target = find(keyValue);
            Tnode<E> current;
            Tnode<E> parent = target.getParent();
            Tnode<E> childL = target.getLeft();
            Tnode<E> childR = target.getRight();

            if (target.getData() == null) {
                return null;

            } else {
            //find node to replace target

                //if the target has a left child that has right children, choose the rightmost
                if (childL != null && childL.getRight() != null) {
                    current = childL.getRight();
                    while (current.getRight() != null) {
                        current = current.getRight();
                    }

                    //if the target has a left child, but that child has no right children, 
                    //use target's left child
                } else if (childL != null) {
                    current = childL;

                    //target has no children to replace it
                } else {
                    System.out.print("\n" + target.getData() + " has been removed.");
                    if (target == parent.getLeft()) {
                        parent.setLeft(null);
                        return target;
                    } else {
                        parent.setRight(null);
                        return target;
                    }
                }

                Tnode<E> moving = new Tnode<E>(current.getData());
                //if target is the root, it has no parent            
                if (target == root) {
                    root = moving;
                    moving.setParent(null);
                } else {
                    moving.setParent(parent);
                }
                //check if target has a left child
                if (childL != null) {
                    moving.setLeft(childL);
                }
                //check if target has a right child
                if (childR != null) {
                    moving.setRight(childR);
                }
                //check if the data being moved has a left child
                if (current.getLeft() != null) {
                    current.getLeft().setParent(current.getParent());

                    if (current.getParent() != null) {
                        current.getParent().setRight(current.getLeft());
                    }
                } else {
                    current.getParent().setRight(null);
                }

                System.out.print("\n" + target.getData() + " has been removed.");
                return target;
            }
        } catch (NullPointerException e) {
            System.out.println("\nThe value " + keyValue + " does not exist in the tree"
                    + " and cannot be deleted.\n");
            return null;
        }
    } //delete
} //BinaryTree

package csu.newman;

//
// Author: Yvonne Newman
// Date: Apr 21, 2015
// Goal: Implements binary search tree
//-----------------------------------------------------------------------
public class Driver {

    public static void main(String[] args) {

        BinaryTree<Integer> tree = new BinaryTree<>(20, 10, 40, 16, 30, 80, 14, 27, 550);
        
        //starting output
        tree.rowWise();
        tree.inOrder();
                
        //remove 27 and show
        System.out.println();
        tree.delete(27);
        tree.rowWise();
        tree.inOrder();

        //remove 20 and show
        System.out.println();
        tree.delete(20);
        tree.rowWise();
        tree.inOrder();

        //remove 40 and show
        System.out.println();
        tree.delete(40);
        tree.rowWise();
        tree.inOrder();
        
        System.out.println();

    }//main
}//Driver

package csu.newman;

//
// Author: Yvonne Newman
// Date: Apr 21, 2015
// Goal: Nodes in binary tree
//-----------------------------------------------------------------------

public class Tnode< E extends Comparable<E>> {

    E data;
    Tnode<E> left;
    Tnode<E> right;
    Tnode<E> parent;

    public Tnode(E data) {
        this.data = data;
        this.left = null;
        this.right = null;
        this.parent = null;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Tnode<E> getLeft() {
        return left;
    }

    public void setLeft(Tnode<E> left) {
        this.left = left;
    }

    public Tnode<E> getRight() {
        return right;
    }

    public void setRight(Tnode<E> right) {
        this.right = right;
    }

    public Tnode<E> getParent() {
        return parent;
    }

    public void setParent(Tnode<E> parent) {
        this.parent = parent;
    }

    public String showTnode() {
        return pointer(this)
                + " [DATA= " + data
                + "\tLEFT=" + pointer(left)
                + "\tRIGHT=" + pointer(right)
                + "\tPARENT=" + pointer(parent)
                + " ]";
    }

    public String pointer(Tnode<E> ptr) {
        if (ptr == null) {
            return "null";
        } else {
//return ptr.getClass() + "@" + Integer.toHexString(ptr.hashCode());
            return Integer.toHexString(ptr.hashCode());
        }
    }

    public int compareTo(Tnode<E> otherNode) {
        return this.getData().compareTo(otherNode.getData());
    }
}//Tnode

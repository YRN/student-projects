
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class Assembler {
	
	static int numBytes = 0; //counts bytes for objectProgram
	static String objLine; // holds one line of the objectProgram

	public static void main(String[] args) throws Exception {
		String asmFile = args[0];// input asm file
		String asmFileName = asmFile.substring(0, asmFile.length() - 4); // input asm file name
		String label = "."; 
		String instruction = "."; 
		// if there is "," in operands, then separate it two parts operandS1
		// and operandS2, else assign operands to operandS1
		String operandS1 = "."; // operand before "," %ELX for "%ELX,RETADR"
		String operandS2 = "."; // operand after "," RETADR for "%ELX,RETADR"		
		String objCode;
		String strTemp;
		
		int opcode = 0;  
		int LOCCTR = 0; // program counter
		int start = 0; // starting location
		int base = 0;
		int pLength = 0; // program length
		int instrLen = 0; //instruction length
		int intTemp;
		int r = 0; //represents base or program counter mode 

		FileInputStream fileName = null;
		File fileName2 = null;
		PrintWriter writeLS = null;
		PrintWriter writeObj = null;

		// registers
		HashMap<Character, Integer> REG = new HashMap<Character, Integer>();
		REG.put('A', 0);
		REG.put('X', 1);
		REG.put('L', 2);
		REG.put('B', 3);
		REG.put('S', 4);
		REG.put('T', 5);
		REG.put('F', 6);

		// saves locations of variables and labels
		HashMap<String, Integer> SYMTAB = new HashMap<String, Integer>();

		// **********PASS ONE************

		try {
			fileName = new FileInputStream(args[0]);

			Scanner input = new Scanner(fileName);

			String line = null;

			while (input.hasNext()) {

				line = input.nextLine();

				if (!line.startsWith(";")) {// this is not a command line

					// parse the line and assign value to corresponding item
					String[] lineArray = new String[4];
					lineArray = parseASM.parse(line);
					label = lineArray[0];
					instruction = lineArray[1];
					operandS1 = lineArray[2];
					operandS2 = lineArray[3];

					// fill the symbol table (SYMTAB)
					if (!SYMTAB.containsKey(label) && label != null) {
	
						SYMTAB.put(label, LOCCTR);
					}

					if (instruction.equals("START")) {
						start = Integer.parseInt(operandS1, 16);
						LOCCTR = start;

					} else if (instruction.equals("END")) {
						pLength = LOCCTR - start;
					} else {
						LOCCTR += parseASM.getOpcode(line)[1];
					}					
				} // end if (!line.startsWith(";"))
			} // end while (Pass One)

			input.close();

			// handle improper input
		} catch (Exception ex0) {
			if (args.length < 1)
				System.err.println("No file was given.");
			else if (args.length > 1)
				System.err.println("Too many arguments were passed.");
			else
				ex0.printStackTrace();
		} finally {
			if (fileName != null) {
				fileName.close();
			}
		}

		// **********PASS TWO************

		// reset LOCCTR
		LOCCTR = 0;

		try {
			fileName2 = new File("./" + asmFile);
			writeLS = new PrintWriter(asmFileName + ".lst");// listing file
			writeObj = new PrintWriter(asmFileName + ".obj");// object file

			Scanner input2 = new Scanner(fileName2);

			String line2 = null;

			while (input2.hasNext()) {
				line2 = input2.nextLine();

				if (line2.startsWith(";")) {
					writeLS.println(line2); 

				} else {
					// parse the line and assign value to corresponding item
					String[] lineArray = new String[4];
					lineArray = parseASM.parse(line2);
					opcode = parseASM.getOpcode(line2)[0];
					instrLen = parseASM.getOpcode(line2)[1];
					label = lineArray[0];
					instruction = lineArray[1];
					operandS1 = lineArray[2];
					operandS2 = lineArray[3];
					strTemp = "";
					intTemp = 0;
					objCode = "";
					r = 0;
					
					if (instruction.equals("START")) {
						start = Integer.parseInt(operandS1, 16);
						LOCCTR = start;
						String progName = String.format("%-5s", label);
						writeLS.println(intToHex(LOCCTR, 6) + "          " + line2);
						writeObj.println("H" + progName + intToHex(LOCCTR, 6) + intToHex(pLength, 6)); // objHeader
						
					} else if (instruction.equals("WORD")) {
							writeLS.println(intToHex(LOCCTR, 6) + "            " + line2);
							codeBuilder( intToHex(LOCCTR, 6), objCode, writeObj);
							LOCCTR += instrLen;
							numBytes += 3;
							
						} else if (instruction.equals("BYTE")) {
							
							if (operandS1.startsWith("C")) {// char byte as C'EOF'
								String CByte = operandS1.substring(2, operandS1.length() - 1);
								int CByte_len = CByte.length();
								
								for (int i = 0; i < CByte.length(); i++) {
								objCode += Integer.toHexString(CByte.charAt(i)).toUpperCase();
								}
								
								writeLS.println(intToHex(LOCCTR, 6) + " " + objCode +"  " + line2);
								codeBuilder( intToHex(LOCCTR, 6), objCode, writeObj);
								LOCCTR = LOCCTR + CByte_len;
								numBytes += CByte_len;
								
							} else if (operandS1.startsWith("X")) {// X'F3'
								String XByte = operandS1.substring(2, operandS1.length() - 1);
								int XByte_len = XByte.length();
								objCode = XByte;
								writeLS.println(intToHex(LOCCTR, 6) + " " + objCode +"      " + line2);
								codeBuilder( intToHex(LOCCTR, 6), objCode, writeObj);
								LOCCTR = LOCCTR + XByte_len / 2;
								numBytes += XByte_len;
							}
						} else if (instruction.equals("RESW")) {
							int decOperand = Integer.parseInt(operandS1);
							writeLS.println(intToHex(LOCCTR, 6) + "         " + line2);
							LOCCTR = LOCCTR + decOperand * 3;
							
						} else if (instruction.equals("RESB")) {
							int decOperand = Integer.parseInt(operandS1);
							writeLS.println(intToHex(LOCCTR, 6) + "         " + line2);
							LOCCTR = LOCCTR + decOperand;
							
						} else if (instruction.equals("BASE") || instruction.equals("NOBASE")) {// directives	
							writeLS.println("              " + line2);
											
						} else if (instruction.equals("END")) {
							writeLS.println(intToHex(LOCCTR, 6) + "            " + line2);
							codeBuilder(intToHex(LOCCTR- instrLen, 6), instruction, writeObj); //pass instruction "END" to print remaining lines to object program
							writeObj.println("E" + intToHex(start, 6)); 
							pLength = LOCCTR - start;
													
						}		
					
					//format 4
					 else if (instrLen == 4) {// extended
						// operation code is substring after "+"
						instruction = instruction.substring(1);

							 if (!operandS2.equals(".")) { //2 operand + extended
								 
									if (operandS1.startsWith("#") && operandS2.startsWith("%")) { //immediate + extended (+MOV #4096, %EAX)
									intTemp = Integer.valueOf(operandS1.substring(1)); // trim #
									objCode = intToHex(opcode + 1, 2) + 1 + intToHex(intTemp, 5);
									
									} else if (!operandS1.startsWith("#") && operandS2.equals("%")) { //LOAD  (+MOV VAR1, %EAX)
											objCode = intToHex(opcode, 2) + 1 + intToHex(SYMTAB.get(operandS1), 5);
											
									} else { //STORE (+MOV %EAX, VAR1)
											objCode =  intToHex(opcode, 2) + 1 + intToHex(SYMTAB.get(operandS2), 5);
									}
							  } else { // one operand (+JSUB)
									objCode = intToHex(opcode + 3, 2) + 1 + intToHex(SYMTAB.get(operandS1), 5);
							  }
							writeLS.println( intToHex(LOCCTR, 6)	+ " " + objCode + " " + line2);
							codeBuilder( intToHex(LOCCTR, 6), objCode, writeObj);
							numBytes += instrLen;
							LOCCTR += instrLen;
					} //end format 4
							
					 else if (instrLen == 3 && !instruction.equals("BYTE")) { // opcode format is 3

						 	if (!operandS2.equals(".")) { //two operand
						 		
									if (operandS1.startsWith("#")) { //LOAD + immediate (MOV #4096, %EAX or #LENGTH, %EBX)
										operandS1 = operandS1.substring(1); // trim #
										
										if ( SYMTAB.containsKey(operandS1)) { //operand is a label
											intTemp = Integer.valueOf(SYMTAB.get(operandS1))- (LOCCTR + instrLen);
											
											if (intTemp <= -2048 || intTemp >= 2047) { //tests if pc or base relative is needed
												r = 4;
												intTemp = Integer.valueOf(SYMTAB.get(operandS1))- base;
												
											} else r = 2;
											objCode = intToHex(opcode + 1, 2) + r + intToHex(intTemp, 3);		
											
										} else { // operand is numeric value
											intTemp = Integer.valueOf(operandS1); // trim # 
											objCode = intToHex(opcode + 1, 2) + r + intToHex(intTemp, 3);
										} 
											if (operandS2.equals("%EBX")) {
												base = Integer.valueOf(SYMTAB.get(operandS1));
											}
									} else if (operandS1.startsWith("%")) { //STORE (MOV %EAX, VAR1)										
										intTemp = Integer.valueOf(SYMTAB.get(operandS2))- (LOCCTR + instrLen);
										
										if (intTemp <= -2048 || intTemp >= 2047) {
										r = 4;
										intTemp = Integer.valueOf(SYMTAB.get(operandS2))- base;
										
										} else r = 2;
										
										objCode = intToHex(opcode + 3, 2) + r + intToHex(intTemp, 3);
										
									} else {  //LOAD (MOV VAR1, %EAX)																			
											intTemp = Integer.valueOf(SYMTAB.get(operandS1)) - (LOCCTR + instrLen);
											
											if (intTemp <= -2048 || intTemp >= 2047) {
												r = 4;
												intTemp = Integer.valueOf(SYMTAB.get(operandS1))- base;
												
											} else r = 2;
											
											objCode = intToHex(opcode + 3, 2) + r + intToHex(intTemp, 3);
											
													if (operandS2.equals("%EBX")) {
														base = Integer.valueOf(SYMTAB.get(operandS1));
											}									
									}
						 	} else if (!operandS1.equals(".") && operandS2.equals(".")){ //one operand
						 		
						 				if (operandS1.startsWith("@")) {// indirect ( J@RETADR)
						 					// operation code is substring after "@"
											operandS1 = operandS1.substring(1);
											intTemp = Integer.valueOf(SYMTAB.get(operandS1)) - (LOCCTR + instrLen); 
											
											if (intTemp <= -2048 || intTemp >= 2047) {
												r = 4;
												intTemp = Integer.valueOf(SYMTAB.get(operandS1))- base;
												
											} else r = 2;
											
											objCode = intToHex(opcode + 2, 2) + r + intToHex(intTemp, 3);
											
						 				} else if (instruction.equals("LDCH") || instruction.equals("STCH")) { // index mode 
						 					intTemp = operandS1.indexOf('[');
						 					strTemp = operandS1.substring(0, intTemp);
						 					intTemp = Integer.valueOf(SYMTAB.get(strTemp)) - (LOCCTR + instrLen); 
						 					
						 					if (intTemp <= -2048 || intTemp >= 2047) {
												r = 4;
												intTemp = Integer.valueOf(SYMTAB.get(strTemp)) - base;
												
											} else r = 2;
						 					
						 					objCode = intToHex(opcode + 3, 2) + Integer.toHexString(8 + r).toUpperCase() +	intToHex(intTemp, 3);
						 					
						 				} else { //simple
						 					
						 					if (operandS1.startsWith("#")) { //indirect (COMP)
												intTemp = Integer.valueOf(operandS1.substring(1)); // trim #
												
												if (intTemp <= -2048 || intTemp >= 2047) {
													r = 4;
													intTemp = Integer.valueOf(SYMTAB.get(operandS1))- base;
													
												} else r = 0;
												
												objCode = intToHex(opcode + 1, 2) + r + intToHex(intTemp, 3);
												
						 					} else { // (TD or JLT) 
						 					intTemp = Integer.valueOf(SYMTAB.get(operandS1)) - (LOCCTR + instrLen);
						 					
						 					if (intTemp <= -2048 || intTemp >= 2047) {
												r = 4;
												intTemp = Integer.valueOf(SYMTAB.get(operandS1))- base;
												
											} else r = 2;
						 					
						 							if (intTemp < 0) { // trim leading F on negative numbers
						 								strTemp = intToHex(intTemp, 4).substring(4);
									 					objCode = intToHex(opcode + 3, 2) + r + strTemp;
									 					
						 							} else {
						 								objCode = intToHex(opcode + 3, 2) + r + intToHex(intTemp, 3);
						 							}
						 					}
						 				}						
						 	} else { // no operands (RSUB)
						 		objCode = intToHex(opcode + 3, 2) + "0000";
						 	}
						 	if (objCode.length() < 6) {
						 		objCode = "0" + objCode;
						 	}						 	
						 	writeLS.println( intToHex(LOCCTR, 6) + " "	+	objCode + " " + line2);
						 	codeBuilder( intToHex(LOCCTR, 6), objCode, writeObj);
							LOCCTR += instrLen;
							numBytes += instrLen;
						 } // end format 3
					
					 //format 2
					 else if (instrLen == 2) {// register mode
						 
						if (operandS2.startsWith("%")) { // two operands
							objCode = intToHex(opcode, 2);
							objCode = objCode + REG.get(operandS1.charAt(2)) + REG.get(operandS2.charAt(2));
							
						} else { // one operand
							objCode = Integer.toHexString(opcode).toUpperCase();
							objCode = objCode + REG.get(operandS1.charAt(2)) + 0;
						}
					 	writeLS.println( intToHex(LOCCTR, 6) + " "	+	objCode + "    " + line2);
					 	codeBuilder( intToHex(LOCCTR, 6), objCode, writeObj);
						numBytes += instrLen;
						LOCCTR += instrLen;
					} //end format 2					
				}
			} // end while (Pass Two)

			input2.close();

		} catch (Exception ex1) {
			ex1.printStackTrace();
		} finally {
			if (writeLS != null)
				writeLS.close();
			if (writeObj != null)
				writeObj.close();
		}
	}// end main

	//add padding 0's into the hex string and makes the letters upper case
			public static String intToHex(int num, int len) {
			String hexNum = "";
			if (len == 6) {
				hexNum = Integer.toHexString(0x1000000 | num).substring(1).toUpperCase();
			} else if (len == 5) {
				hexNum = Integer.toHexString(0x100000 | num).substring(1).toUpperCase();
			} else if (len == 4) {
				hexNum = Integer.toHexString(0x10000 | num).substring(1).toUpperCase();
			} else if (len == 3) {
				hexNum = Integer.toHexString(0x1000 | num).substring(1).toUpperCase();
			} else if (len == 2) {
				hexNum = Integer.toHexString(0x100 | num).substring(1).toUpperCase();
			}

			return hexNum;
		}
	
//helps to build and format the object program
//a new line is started when the length of the object program line tries to exceed 60 characters 
//numBytes is reset to begin counting bytes in the next line.
//the formatted text is written to an object file
	public static void codeBuilder(String location, String objCode, PrintWriter writeObj) {
		
		if (numBytes == 0) {
			objLine = "T" + location + "." + objCode;
			
		} else if (numBytes < 27) {
			objLine += objCode;
			
			 if (objCode.equals("END")) {
				 objLine = objLine.replace(".", intToHex(numBytes, 2));
					objLine = objLine.substring(0, (objLine.length() - 3));
				writeObj.println(objLine);
			}
			
		} else {
			objLine = objLine.replace(".", intToHex(numBytes, 2));
			writeObj.println(objLine);
			objLine = "T" + location + "." + objCode;
			numBytes = 0;
		} 
	}// end codeBuilder
} // end Assembler



import java.util.HashMap;

//parse ASM input line
public class parseASM {

	/*
	 * parse method for input line return array after
	 * parsing:[0]-label,[1]-instruction(mnemonic opcode) [3]-operands before
	 * ",", [4]-operands after "," (%ELX,RETADR) If operands only one block,
	 * [4]-null if instruction is null, jump to next line if instruction is MOV,
	 * assign opcode according [3] and [4]
	 */
	public static String[] parse(String str) {
		
		String[] newLineArray = new String[4];

		// eliminate whitespace from both before and end
		String line = str.trim();
		
		for (int i = 0; i < newLineArray.length; i++) {
			newLineArray[i] = "." ; //stop null pointer exception by filling array with place-holders
		}

		// cut anything after ";"
		if (line.startsWith(";")) {
			line = null;
		} else if (line.contains(";")) {
			line = line.substring(0, line.indexOf(";"));
		}

		// convert string to array
		if (line != null) {
			line = line.replaceAll("\\s+", " ");
		    String[] strArray = line.split(" ");
			int len = strArray.length;

			if (len == 1){//no label && operands
				newLineArray[1] = strArray[0];
			}
			else if (len == 2) {// no label
				newLineArray[1] = strArray[0];
				newLineArray[2] = strArray[1];
				
				// analysis operands for MOV instruction
				if (newLineArray[2].contains(",")) {
					String operandsArray[] = newLineArray[2].split(",");
					newLineArray[2] = operandsArray[0];// operands before ","
					newLineArray[3] = operandsArray[1];// operands after ","
				}
			} else if (len == 3) {// label, instruction, operands
				newLineArray[0] = strArray[0];
				newLineArray[1] = strArray[1];
				newLineArray[2] = strArray[2];
				
				// analysis operands for MOV instruction
				if (newLineArray[2].contains(",")) {
					String operandsArray[] = newLineArray[2].split(",");
					newLineArray[2] = operandsArray[0];// operands before ","
					newLineArray[3] = operandsArray[1];// operands after ","
				}
			}
		}
		return newLineArray;
	}//end of parse method
	
	/*getOpcodeMOV method return the opcode for MOV instruction*/
	public static int getOpcodeMOV(String operandS1, String operandS2){
		
		/*OPCODE for MOV instruction*/
        HashMap<String, Integer> MOVTAB = new HashMap<String, Integer>();
        MOVTAB.put("LDA", 0x00);
        MOVTAB.put("LDB", 0x68);
        MOVTAB.put("LDL", 0x08);
        MOVTAB.put("LDS", 0x6C);
        MOVTAB.put("LDT", 0x74);
        MOVTAB.put("LDX", 0x04);
        MOVTAB.put("STA", 0x0C);
        MOVTAB.put("STB", 0x78);
        MOVTAB.put("STL", 0x14);
        MOVTAB.put("STS", 0x7C);
        MOVTAB.put("STT", 0x84);
        MOVTAB.put("STX", 0x10);
        
		int opcode = 0;
		
		if(operandS1.equals("%EAX")){//STA
			opcode = MOVTAB.get("STA");
		}
		else if(operandS1.equals("%EBX")){//STB
			opcode = MOVTAB.get("STB");
		}
		else if(operandS1.equals("%ELX")){//STL
			opcode = MOVTAB.get("STL");
		}
		else if(operandS1.equals("%ESX")){//STS
			opcode = MOVTAB.get("STB");
		}
		else if(operandS1.equals("%ETX")){//STT
			opcode = MOVTAB.get("STT");
		}
		else if(operandS1.equals("%EXX")){//STX
			opcode = MOVTAB.get("STX");
		}
		else if(operandS2.equals("%EAX")){//LDA
			opcode = MOVTAB.get("LDA");
		}
		else if(operandS2.equals("%EBX")){//LDB
			opcode = MOVTAB.get("LDB");
		}
		else if(operandS2.equals("%ELX")){//LDL
			opcode = MOVTAB.get("LDL");
		}
		else if(operandS2.equals("%ESX")){//LDS
			opcode = MOVTAB.get("SSB");
		}
		else if(operandS2.equals("%ETX")){//LDT
			opcode = MOVTAB.get("LDT");
		}
		else if(operandS2.equals("%EXX")){//LDX
			opcode = MOVTAB.get("LDX");
		}
		else {
			opcode = -1;
		}   
        return opcode;
	}
	
	/** getOpcode method
	 * parse the input line and return the opcode and length of instruction (1, 2, 3, 4 or n)
	 * @param line  The asm file command line
	 * @return 	    Integer array A with two value, 
	 * 				A[0] is the opcode, A[1] is the length of instruction	
	 * */
	public static int[] getOpcode(String line){
		String instruction = ""; // instruction like MOV or ADD
		
		//if there is "," in operands, then separate it two parts operandS1
		//and operandS2, else assign operands to operandS1 
		String operandS1 = ""; // operand before "," %ELX for "%ELX,RETADR"
		String operandS2 = ""; // operand after "," RETADR for "%ELX,RETADR"
		int opcode = 0; //instruction opcode
		int instrLen = 0x0; // the length of instruction
		
		// assigns opcodes to instructions
		HashMap<String, Integer> OPTAB = new HashMap<String, Integer>();
		OPTAB.put("ADD", 0x18);
		OPTAB.put("ADDR", 0x90);
		OPTAB.put("CLEAR", 0xB4);
		OPTAB.put("COMP", 0x28);
		OPTAB.put("COMPR", 0xA0);
		OPTAB.put("DIV", 0x24);
		OPTAB.put("J", 0x3C);
		OPTAB.put("JEQ", 0x30);
		OPTAB.put("JGT", 0x34);
		OPTAB.put("JLT", 0x38);
		OPTAB.put("JSUB", 0x48);
		OPTAB.put("LDCH", 0x50);
		OPTAB.put("MUL", 0x20);
		OPTAB.put("RD", 0xD8);
		OPTAB.put("RSUB", 0x4C);
		OPTAB.put("STCH", 0x54);
		OPTAB.put("SUB", 0x1C);
		OPTAB.put("SUBR", 0x94);
		OPTAB.put("TD", 0xE0);
		OPTAB.put("TIX", 0x2C);
		OPTAB.put("TIXR", 0xB8);
		OPTAB.put("WD", 0xDC);

		if (!line.startsWith(";")){//this is not a command line
			
		// parse the line and assign value to corresponding item
		String[] lineArray = new String[4];
		lineArray = parse(line);
		//printArray(lineArray);//print each line after parse
		instruction = lineArray[1];
		operandS1 = lineArray[2];
		operandS2 = lineArray[3];
		
		if (instruction.equals("START")) {
			instrLen = 0;
			opcode = -1;
		}
		else if(instruction.startsWith("+")){//opcode format is 4
				//operation code is substring after "+"
				instruction = instruction.substring(1);
				
				if (instruction.endsWith("R")){//opcode is format 2
					System.out.println("Error! The opcode is format 2");
				}
				else if (instruction.equals("MOV")){
					instrLen = 4;
					opcode = getOpcodeMOV(operandS1, operandS2);
				}
				else if (OPTAB.containsKey(instruction)){
					instrLen = 4;
					opcode = OPTAB.get(instruction);
				}else{//opcode is not exist
					System.out.println("Error! The +opcode is not exist!");
				}
			}
		else if(instruction.endsWith("R") && 
				OPTAB.containsKey(instruction)){//opcode format is 2
			    instrLen = 2;
				opcode = OPTAB.get(instruction);
			}
		else if(instruction.equals("MOV")){
				//opcode format is 3
			    instrLen = 3;
				opcode = getOpcodeMOV(operandS1, operandS2);
			}
		else if(OPTAB.containsKey(instruction)){
			//opcode format is 3
			instrLen = 3;
			opcode = OPTAB.get(instruction);
		}
		else if(instruction.equals("WORD")){
			    instrLen = 3;
				opcode = -1;
			}
		else if(instruction.equals("BYTE")){
				if (operandS1.startsWith("C")){//char byte as C'EOF'
					String CBtye = operandS1.substring(2, operandS1.length()-1);
					int CBtye_len = CBtye.length();
					instrLen = CBtye_len;
					opcode = -1;
				}
				else if (operandS1.startsWith("X")){
					String XBtye = operandS1.substring(2, operandS1.length()-1);
					int CBtye_len = XBtye.length();
					instrLen = CBtye_len/2;
					opcode = -1;
				}
			}
		else if(instruction.equals("RESW")){
				int decOperand = Integer.parseInt(operandS1);
				instrLen = decOperand * 3;
				opcode = -1;
			}
		else if(instruction.equals("RESB")){
				int decOperand = Integer.parseInt(operandS1);
				instrLen = decOperand;
				opcode = -1;
			}
		else if(instruction.equals("BASE") ||
				instruction.equals("NOBASE")){//directives
			instrLen = 0;
			opcode = -1;
			}
		else if(instruction.equals("END")){
			instrLen = 0;
			opcode = -1;
		}
		else{
				System.out.println("Error! Wrong instructions!");
			}
			
		}//end if (!line.startsWith(";"))			
		
		//return value opcode and location counter for the instruction
		int[] opcodeVal = new int[2];
		opcodeVal[0] = opcode; 
		opcodeVal[1] = instrLen;
		
		return opcodeVal;	
	}
}


package csu.newman;

// Author: Yvonne Newman
// Date: Apr 13, 2015
// Goal: sorts series of objects using heap sort
//-----------------------------------------------------------------------
public class Driver< E extends Comparable<E>> {

    public static void main(String[] args) {

        int[] list = {2, 9, 5, 4, 8, 1, 6, 7};

        GeometricObject[] list2 = {
            new Circle(2),
            new Circle(9),
            new Circle(5),
            new Circle(4),
            new Circle(8),
            new Circle(1),
            new Circle(6),
            new Circle(7),
            new Rectangle(1, 2)};

        MaxHeap intHeap = new MaxHeap();
        MaxHeap geoHeap = new MaxHeap();

        //form int heap
        for (int e : list) {
            intHeap.add(e);
        }
        intHeap.show();

        //create and print int array, sorted in descending order
        System.out.print("Heap Sorted Descending: \n[");
        int fullRun = intHeap.getSize();
        for (int i = 0; i < fullRun; i++) {
            int pop = (int) intHeap.remove();
            list[i] = pop;
            if (i != fullRun - 1) {
                System.out.print(list[i] + ", ");
            } else {
                System.out.print(list[i]);
            }
        }
        System.out.print("]\n");

        //form geometric object heap
        for (GeometricObject e : list2) {
            geoHeap.add(e);
        }
        geoHeap.show();

        //rearrange array in ascending order by area
        System.out.print("Heap Sorted Ascending: \n");
        fullRun = geoHeap.getSize() - 1;
        for (int j = fullRun; j >= 0; j--) {
            GeometricObject pop = (GeometricObject) geoHeap.remove();
            list2[j] = pop;
        }
        //print newly arranged arry
        for (GeometricObject e : list2) {
            System.out.print(e + "\n");
        }
    }
}

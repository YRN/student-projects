package csu.newman;

//
// Author: Yvonne Newman
// Date: Jan 29, 2015
// Goal: 
//-----------------------------------------------------------------------

public class Rectangle extends GeometricObject {

    private double height;
    private double width;

    public Rectangle() {
        this.height = 2;
        this.width = 2;
    }

    public Rectangle(double h, double w) {
        this.height = h;
        this.width = w;
    }

    public Rectangle(String color, boolean filled, double h, double w) {
        super(color, filled);
        this.height = h;
        this.width = w;
    } 

    public double getHeigth() {
        return height;
    }

    public void setHeigth(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public double getArea() {
        double area = (double) (width * height);
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = (double) ((width * 2) + (height * 2));
        return perimeter;
    }
    @Override
    public String getIdentity() {
        return "RECTANGLE";
    }
    @Override
    public String toString() {
        return super.toString() + "\nRectangle [ " + "Height= " + height  + " Width= " +
          width + " Area= " + (double) Math.round(getArea()) + 
                " Perimeter= "+ (double) Math.round(getPerimeter()) + " ]\n"; 
    }
    }

package csu.newman;

//
// Author: Yvonne Newman
// Date: Jan 29, 2015
// Goal: 
//-----------------------------------------------------------------------

public class Circle extends GeometricObject {

    private double radius;

    public Circle() {
        this.radius = 1;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        if (radius < 0) {
            radius = 0;
            System.out.println("This radius is impossible.");
        }
        this.radius = radius;
    }

    @Override
    public double getArea() {
        double area = (double) (radius * radius * Math.PI);
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = (double) (Math.PI * radius * 2);
        return perimeter;
    }
    
    @Override
    public String getIdentity() {
        return "CIRCLE";
    }

    @Override
    public String toString() {
        return super.toString() + "\nCircle [ " + "radius= " + radius  +
               " Area= " + (double) Math.round(getArea()) + 
                " Perimeter= "+ (double) Math.round(getPerimeter()) + " ]\n"; 
    }
    }

package csu.newman;

import java.util.ArrayList;

// Author: Yvonne Newman
// Date: Apr 13, 2015
// Goal: show, create and dismantle heaps
//-----------------------------------------------------------------------
public class MaxHeap< E extends Comparable<E>> {

    private ArrayList<E> list;

    public MaxHeap() {
        this.list = new ArrayList<>();
    }

    public MaxHeap(ArrayList<E> list) {
        this.list = list;
    }

    public ArrayList<E> getList() {
        return list;
    }

    public int getSize() {
        return list.size();
    }

    public void add(E newItem) {
        //add new item at end and index position
        list.add(newItem);
        int tarPos = list.indexOf(newItem);

        //while target isn't at the top of the list
        while (tarPos > 0) {
            int parentPos = ((tarPos - 1) / 2);
            E parent = list.get(parentPos);
            E target = list.get(tarPos);
            //check target's parent, swap if target is bigger
            if (target.compareTo(parent) > 0) {
                list.set(parentPos, target);
                list.set(tarPos, parent);
            } else {
                //target is in it's proper position
                break;
            }
        }
    }

    public E remove() {
        //take of root (the first of the array)
        //shuffle up array, last moves to first and filters down from there

        //check if there's something to remove
        if (list.isEmpty()) {
            return null;

        } else {

            //last place
            int last = (list.size() - 1);

            //item to move to top
            E target = list.get(last);

            //starting point
            int tarPos = 0;

            //item to remove
            E pop = list.get(0);

            //set the new top and take off the end
            list.set(0, target);
            list.remove(last);
            last--;

            //while still within the array
            while (tarPos < last) {
                int childLPos = ((2 * tarPos) + 1);
                int childRPos = childLPos + 1;
                E hold;

                //if both children exist and are bigger, stop
                if (childLPos < list.size() && childRPos < list.size()) {
                    if ((target.compareTo(list.get(childLPos)) > 0)
                            && (target.compareTo(list.get(childRPos)) > 0)) {
                        break;

                        //if left is biggest, swap
                    } else if (list.get(childLPos).compareTo(list.get(childRPos)) > 0) {
                        hold = list.get(childLPos);
                        list.set(childLPos, target);
                        list.set(tarPos, hold);
                        tarPos = list.indexOf(target);

                        //if right is the biggest, swap
                    } else {
                        hold = list.get(childRPos);
                        list.set(childRPos, target);
                        list.set(tarPos, hold);
                        tarPos = list.indexOf(target);
                    }
                    //if the left child exists
                } else if (childLPos < list.size()) {
                    //if the left child is bigger, swap
                    if (target.compareTo(list.get(childLPos)) < 0) {
                        hold = list.get(childLPos);
                        list.set(childLPos, target);
                        list.set(tarPos, hold);
                        tarPos = list.indexOf(target);
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            return pop;
        }
    }

    public void show() {
//print in a way that works reasonably well for all data types
        System.out.print("\nThe heap: \n");
        for (E e : list) {
        System.out.print(e + " ");
        }
        System.out.println("\n");
    }
   }

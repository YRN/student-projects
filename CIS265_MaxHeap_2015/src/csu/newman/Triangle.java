package csu.newman;

//
// Author: Yvonne Newman
// Date: Feb 14, 2015
// Goal: Create a triangle object.
//-----------------------------------------------------------------------
public class Triangle extends GeometricObject {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle() {
        this.sideA = 2;
        this.sideB = 2;
        this.sideC = 2;
    }

    public Triangle(double sideA, double sideB, double sideC)
            throws IllegalArgumentException {
        try {
            if (((sideA + sideB) > sideC) && ((sideB + sideC) > sideA)
                    && ((sideA + sideC) > sideB)) {
                this.sideA = sideA;
                this.sideB = sideB;
                this.sideC = sideC;
            } else {
                throw new IllegalArgumentException("Bad lengths.");
            }

        } catch (IllegalArgumentException ex) {
            System.out.println("This is not a triangle.");
        }
    }

    public Triangle(String color, boolean filled, double sideA, double sideB, double sideC)
            throws IllegalArgumentException {
        super(color, filled);
        try {
            if (((sideA + sideB) > sideC) && ((sideB + sideC) > sideA)
                    && ((sideA + sideC) > sideB)) {
                this.sideA = sideA;
                this.sideB = sideB;
                this.sideC = sideC;
            } else {
                throw new IllegalArgumentException("Side lengths not possible.");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("These lengths do not make a triangle.");
        }
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getSideC() {
        return sideC;
    }

    public void setSideA(double sideA) throws IllegalArgumentException {
        try {
            if (((sideA + sideB) > sideC) && ((sideB + sideC) > sideA) && ((sideA + sideC) > sideB)) {
                this.sideA = sideA;
            } else {
                throw new IllegalArgumentException("Not a triangle.");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Not a possible length for side A.");
        }
    }

    public void setSideB(double sideB) throws IllegalArgumentException {
        try {
            if (((sideA + sideB) > sideC) && ((sideB + sideC) > sideA) && ((sideA + sideC) > sideB)) {
                this.sideB = sideB;
            } else {
                throw new IllegalArgumentException("Not a triangle.");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Not a possible length for side B.");
        }
    }

    public void setSideC(double sideC) throws IllegalArgumentException {
        try {
            if (((sideA + sideB) > sideC) && ((sideB + sideC) > sideA) && ((sideA + sideC) > sideB)) {
                this.sideC = sideC;
            } else {
                throw new IllegalArgumentException("Not a triangle.");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println("Not a possible length for side C.");
        }
    }

    @Override
    public double getArea() {
        double p = this.getPerimeter();
        double area = (double) Math.sqrt((p) * (p - sideA) * (p - sideB) * (p - sideC));
        return area;
    }

    @Override
    public double getPerimeter() {
        double perimeter = (double) (sideA + sideB + sideC);
        return perimeter;
    }

    @Override
    public String getIdentity() {
        return "TRIANGLE";
    }

    @Override
    public String toString() {
        String message;
        if (((sideA + sideB) > sideC) && ((sideB + sideC) > sideA)
                && ((sideA + sideC) > sideB)) {
            message = super.toString() + "Triangle [ " + " sideA= " + sideA
                    + " sideB= " + sideB + " sideC= " + sideC
                    + " Area= " + (double) Math.round(getArea())
                    + " Perimeter= " + (double) Math.round(getPerimeter()) + " ]";
        } else {
            message = " Not a Triangle";
        }
        return message;
    }
}

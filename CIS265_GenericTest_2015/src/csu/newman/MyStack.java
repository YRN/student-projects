package csu.newman;

//
import java.util.ArrayList;

// Author: Yvonne Newman
// Date: Mar 5, 2015
// Goal: 
//-----------------------------------------------------------------------
public class MyStack<E> {

    //class vars
    private ArrayList<E> list;

    //constructor
    public MyStack() {
        list = new ArrayList<>();
    }

    //user methods
    public void push(E data) {
        list.add(data);
    }

    public E pop() {
        if (list.size() == 0) {
            return null;
        } else {
            return list.remove(list.size() - 1);
        }
    }

    public boolean isEmpty() {
        if (list.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public E peek() {
        if (list.size() == 0) {
            return null;
        } else {
            return list.get(list.size() - 1);
        }
    }

    @Override
    public String toString() {
        return "MyStack{" + "list=" + list + '}';
    }
}


package csu.newman;

//
// Author: Yvonne Newman
// Date: Mar 5, 2015
// Goal: Explore inplementation of a GENERIC stack.
//-----------------------------------------------------------------------

public class Driver {

    public static void main(String[] args) {
        
        MyStack<Integer> stk1 = new MyStack<Integer>();
        stk1.push(123);
        stk1.push(456);
        
        MyStack<GeometricObject> stk2 = new MyStack<GeometricObject>();
        stk2.push( new Circle("red", true, 1));
        
        System.out.println(stk1);
        System.out.println(stk2);
            
    }
}

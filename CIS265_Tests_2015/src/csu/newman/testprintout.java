
package csu.newman;

//
// Author: Yvonne Newman
// Date: Nov 8, 2015
// Goal: 
//-----------------------------------------------------------------------

public class testprintout {


    public  void main(String[] args) {
String line1 = "; this is a comment";
		String line2 = "COPY     START   0";
		String line3 = "FIRST    MOV     %ELX,RETADR  ; save Return Address";
		String line4 = "         JEQ     ENDFIL";
		String line5 = "CLOOP   +JSUB    RDREC";
		String line6 = "         J       @RETADR      ; Return back to the caller";

		
		printArray(parse(line1));
		printArray(parse(line2));
		printArray(parse(line3));
		printArray(parse(line4));
		printArray(parse(line5));
		printArray(parse(line6));	
	}
	
	/*print array*/
	public static void printArray(String[] strArray){
		for (int i=0; i < strArray.length; i++){
			System.out.print(strArray[i] + " ");
		}
		System.out.println();
	}
	
    
    /*parse method for input line
     * return array after parsing:[0]-label,[1]-instruction(mnemonic opcode)
     * [3]-operands before ",", [4]-operands after "," (%ELX,RETADR)
     * If operands only one block, [4]-null
     * if instruction is null, jump to next line
     * if instruction is MOV, assign opcode according [3] and [4]*/
    public String[] parse(String str) {
        //return array
        String[] newLineArray = new String[4];

        //eliminate whitespace from both before and end
        String line = str.trim();

        //cut anything after ";"
        if (line.startsWith(";")) {
            line = null;
        } else if (line.contains(";")) {
            line = line.substring(0, line.indexOf(";"));
        }

        //convert string to array
        if (line != null) {

            //replace continuous space with one space
            line = line.replaceAll("\\s+", " ");

            //split string by space
            String[] strArray = line.split(" ");

            int len = strArray.length;

            if (len == 2) {//no label
                newLineArray[1] = strArray[0];
                newLineArray[2] = strArray[1];
//
//                instruction = newLineArray[1];
//                operand = newLineArray[2];

            } else if (len == 3) {//label, instruction, operands
                newLineArray[0] = strArray[0];
                newLineArray[1] = strArray[1];
                newLineArray[2] = strArray[2];

//                label = newLineArray[0];
//                instruction = newLineArray[1];
//                operand = newLineArray[2];
            }

            //analysis operands for MOV instruction
            if (newLineArray[2].contains(",")) {
                String operandsArray[] = newLineArray[2].split(",");
                newLineArray[2] = operandsArray[0];//operands before ","
                newLineArray[3] = operandsArray[1];//operands after ","
                String ch;
                
                //LOAD #, %E_X
                if (operandsArray[1].contains("X")) {
                    str = operandsArray[1];
                    ch = str.substring(str.length() - 1);
                }
                 //STORE %E_X, #
                if (operandsArray[0].contains("X")) {
                    
                }
                    
            }
        }
        return newLineArray;
    }
}

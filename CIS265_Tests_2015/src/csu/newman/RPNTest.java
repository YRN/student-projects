package csu.newman;

//
import java.util.Stack;
import java.util.StringTokenizer;

// Author: Yvonne Newman
// Date: Apr 16, 2015
// Goal: 
//-----------------------------------------------------------------------
public class RPNTest {

    public static void main(String[] args) {
        String expression = "1-(2+3)*4";

        String rpnSolution = infix2Postfix(expression);

        System.out.println("infix exp: " + expression);
        System.out.println("infix exp: " + rpnSolution);

    }

    private static String infix2Postfix(String expression) {
        Stack<String> stack = new Stack<>();
        String solution = "";
        StringTokenizer tokens = new StringTokenizer(expression, "*/+-() ", true);

        while (tokens.hasMoreTokens()) {
            String current = tokens.nextToken().trim();
            if (current.length() == 0) {
                continue;
            } else if (current.equals("+") || current.equals("-")) {
                while (!(stack.isEmpty()) && (stack.peek().equals("*"))
                        || (stack.peek().equals("/"))
                        || (stack.peek().equals("+"))
                        || (stack.peek().equals("-"))) {
                    solution += stack.pop();
                }
                stack.push(current);
            } else if (current.equals("*") || current.equals("/")) {
                while (!stack.isEmpty() && (stack.peek().equals("*"))
                        || (stack.peek().equals("/"))) {
                    solution += stack.pop();
                }
                stack.push(current);
            } else if (current.equals("(")) {
                stack.push(current);
            } else if (current.equals(")")) {
                while (!stack.peek().equals("(")) {
                    solution += stack.pop();
                }
                stack.pop();
            } else {
                solution += current;
            } 
            while (!stack.isEmpty()) {
                solution += stack.pop();
            }
        }
        return solution;
    }
}

package csu.newman;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

// Author: Yvonne Newman
// Date: Mar 31, 2015
// Goal: read data from a random accesss mode file.
//-----------------------------------------------------------------------
public class Mapping {

    static final String FILEPATH = "C:\\Users\\Yvii\\Desktop\\Code\\customerfile.txt";

    public static void main(String[] args) throws FileNotFoundException {

        TreeMap<String, Integer> mapTable = new TreeMap<>();

        createMap(FILEPATH);

        int position = mapTable.get("aaa");

        String record = getRecfromFile(FILEPATH, position, 23);
        //where, which one, how long is that one
        System.out.println(record);
    }

    private static String getRecfromFile(String filepath, int position, int recLength) {

        try {
            RandomAccessFile file = new RandomAccessFile(filepath, "r");
            file.seek((recLength + 2) * position); 
                    //move reader to right spot (carriage return etc)
            byte[] buffer = new byte[recLength]; 
                        //like a ruler, it makes an array of the stuff inside
            file.read(buffer); //read that ruler
            file.close();
            return new String(buffer); //print the array

        } catch (Exception e) {
            e.printStackTrace();
            return "***";
        }
    }

    private static Map<String, Integer> createMap(String filepath) 
            throws FileNotFoundException {

        TreeMap<String, Integer> result = new TreeMap<>();

        try {
            Scanner input = new Scanner(new File(FILEPATH));

            int position = 0;

            while (input.hasNext()) {
                String record = input.nextLine();
                String[] tokens = record.split(" ");
                result.put(tokens[0], position);
                position++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }
}

package csu.newman;

//
// Author: Yvonne Newman
// Date: Mar 17, 2015
// Goal: 
//-----------------------------------------------------------------------

public class Node< T extends Comparable<T> > implements Comparable <Node<T> > {

    T data;
    Node<T> next;

    public Node(T data) {
        this.data = data;
        this.next = null;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    //user-defined method
    public void showNode() {
        String result = "\nREF: \t" + this
                + "\nDATA: \t" + this.getData()
                + "\nNEXT: \t" + this.getNext();
        System.out.println(result);
    }

    public String getAddress() {
        return Integer.toHexString(this.hashCode())
                + " Node [ data = " + next;
    }

//    @Override
//    public String toString() {
//        return "Node{" + "data = " + data + ", next = " + next + '}';
//    }

    @Override
    public int compareTo(Node<T> other) {
        return this.getData().compareTo(other.getData());
    }
}

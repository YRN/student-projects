package csu.newman;

//
// Author: Yvonne Newman
// Date: Mar 24, 2015
// Goal: 
//-----------------------------------------------------------------------

public class Driver {

    public static void main(String[] args) {

        MyLinkedList<String> dlist = new MyLinkedList<>();
        
        dlist.add( new Node<> ("A"));
        dlist.add( new Node<> ("B"));
        dlist.add( new Node<> ("C"));
        dlist.add( new Node<> ("D"));
        dlist.add( new Node<> ("E"));
        dlist.add( new Node<> ("F"));
        dlist.add( new Node<> ("G"));
        
        dlist.showList();
        
        if (dlist.find("C") != null) {
        dlist.addAfter(new Node<> ("X"), dlist.find("C"));
        }
        
        dlist.showList();
        
        dlist.removeNext(dlist.find("E"));
        
        dlist.showList();
        
        dlist.addFirst(new Node<> ("F"));
        
        dlist.showList();
        
        if (dlist.find("B") != null) {
        dlist.replace(new Node<> ("Z"), dlist.find("B"));
        }
                
        dlist.showList();

    }
}

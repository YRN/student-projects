package csu.newman;

//
import java.util.Arrays;

// Author: Yvonne Newman
// Date: Apr 2, 2015
// Goal: 
//-----------------------------------------------------------------------
public class SortingTests {

    public static void main(String[] args) {

        int[] array = {2, 4, 5, 9};
        int[] array2 = {1, 6, 7, 8};

//        System.out.print("before: " + Arrays.toString(array));
//       bubbleSort(array);
//        System.out.print("\nafter:  " + Arrays.toString(array));
        
        System.out.print("before: " + Arrays.toString(array) + Arrays.toString(array2));
        int[] merged = merge(array, array2);
        System.out.print("\nafter:  " + Arrays.toString(merged));
    }

    //quadradic O(n^2)
    public static void bubbleSort(int[] a) {

        int temp;
        Boolean again = true;

        for (int i = 0; i < a.length && again; i++) {
            again = false;

            for (int j = 0; j < a.length - 1; j++) {
                if (a[j] > a[j + 1]) {
                    temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    again = true;
                }
            }
        }
    }

    private static int[] merge(int[] a, int[] b) {

        int size = a.length + b.length;

        int[] c = new int[size];

        int i1 = 0;
        int i2 = 0;
        int i3 = 0;

        //as long as we're within BOTH arrays
        while (i1 < a.length && i2 < b.length) {

            //if the first array is smaller
            if (a[i1] <= b[i2]) {

                //place in c is the first array's number and c and a move ahead
                c[i3++] = a[i1++];

                //place in c is the second array's number and c and b move ahead
            } else {
                c[i3++] = b[i2++];
            }
        }

        //still leftover numbers in a, move c up and work through a
        while (i1 < a.length) {
            c[i3++] = a[i1++];
        }

        //still leftover numbers in b, move c up and work through b
        while (i2 < b.length) {
            c[i3++] = b[i2++];
        }
        return c;
    }
    
    
}

package csu.newman;

import java.util.Date;

// Author: Yvonne Newman
// Date: Jan 29, 2015
// Goal: 
//------------------------------------------------------------------

public abstract class GeometricObject_1 implements Comparable <GeometricObject> {

    private String color = "white";
    private boolean filled = true;
    private java.util.Date dateCreated;

    protected GeometricObject_1() {
        this.dateCreated = getDateCreated();
    }

    public GeometricObject_1(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
        this.dateCreated = getDateCreated();
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    private Date getDateCreated() {
        java.util.Date date = new java.util.Date();
        return date;
    }

    String getReference() {
        return this.getClass().getCanonicalName() + "@" + Integer.toHexString(this.hashCode());
    }

    // implimentation orders
    public abstract double getArea();

    public abstract double getPerimeter();
    
    public abstract String getIdentity();

    @Override
    public String toString() {
        return "GeometricObject [" + "color= " + color + ", filled= "
                + filled + ", dateOfCreation= " + dateCreated + "]\n";
    }
        @Override
    public int compareTo(GeometricObject other) {
        if (this.getArea() > other.getArea()) {
            return 1;
        } else if (this.getArea() < other.getArea()) {
            return -1;
        } else if (this.getArea() == other.getArea()) {
            return 0;
        }
        return 0;
    }
    }

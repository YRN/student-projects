package csu.newman;

//
import java.util.Arrays;

// Author: Yvonne Newman
// Date: Apr 9, 2015
// Goal: implement sort/merge algorithm
//-----------------------------------------------------------------------
public class SplitMergeSort {

    public static void main(String[] args) {

        int[] a = {2, 9, 5, 4, 8, 1, 6, 7, 0};
        System.out.println("Before: " + Arrays.toString(a));

        bisect(a);

        System.out.println("\n\nAfter: " + Arrays.toString(a));
    }

    private static int[] merge(int[] a, int[] b) {

        System.out.print("merging: " + Arrays.toString(a) + Arrays.toString(b));

        int size = a.length + b.length;

        int[] c = new int[size];

        int i1 = 0;
        int i2 = 0;
        int i3 = 0;

        //as long as we're within BOTH arrays
        while (i1 < a.length && i2 < b.length) {

            //if the first array is smaller
            if (a[i1] <= b[i2]) {

                //place in c is the first array's number and c and a move ahead
                c[i3++] = a[i1++];

                //place in c is the second array's number and c and b move ahead
            } else {
                c[i3++] = b[i2++];
            }
        }

        //still leftover numbers in a, move c up and work through a
        while (i1 < a.length) {
            c[i3++] = a[i1++];
        }

        //still leftover numbers in b, move c up and work through b
        while (i2 < b.length) {
            c[i3++] = b[i2++];
        }
        return c;
    }

    private static void bisect(int[] a) {

        System.out.println("\n" + Arrays.toString(a));
        int n = a.length;

        if (n > 1) {
            int[] firstHalf = new int[n / 2];
            int[] secondHalf = new int[n - (n / 2)];
            System.arraycopy(a, 0, firstHalf, 0, (n / 2));
            System.arraycopy(a, n/2, secondHalf, 0, (n - (n / 2)));

            System.out.println("\n" + Arrays.toString(firstHalf));
            System.out.println("\n" + Arrays.toString(secondHalf));

            bisect(firstHalf);
            bisect(secondHalf);
            
            int[] combo = merge(firstHalf, secondHalf);
            System.out.println("\nMerged: " + Arrays.toString(combo));
            
            System.arraycopy(combo, 0, a, 0, n);
        }
    }
}

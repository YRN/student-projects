package csu.newman;

//
// Author: Yvonne Newman
// Date: Mar 24, 2015
// Goal: 
//-----------------------------------------------------------------------
//can't pass ints because they are primitives types and not comparable

public class MyLinkedList<T extends Comparable<T>> {

    //class variables
    private Node<T> first;
    private Node<T> last;
    private int count;

    //constructor
    public MyLinkedList() {
        first = null;
        last = null;
        count = 0;
    }

    //accessors
    public Node<T> getFirst() {
        return first;
    }

    public void setFirst(Node<T> first) {
        this.first = first;
    }

    public Node<T> getLast() {
        return last;
    }

    public void setLast(Node<T> last) {
        this.last = last;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

//user-defined methods
    public void add(Node<T> newNode) {

        if (count == 0) {
            first = newNode;
            last = newNode;
            count = 1;

        } else {
            last.setNext(newNode);
            last = newNode;
            count++;
        }
    }

    public void addFirst(Node<T> newNode) {

        if (count == 0) {
            first = newNode;
            last = newNode;
            count = 1;

        } else {
            newNode.setNext(first);
            first = newNode;
                count++;
        }
    }
    
        public void addAfter(Node<T> newNode, Node<T> after) {

        if (count == 0) {
            first = newNode;
            last = newNode;
            count = 1;

        } else {
         Node<T> after2 = after.getNext();
          newNode.setNext(after2);
          after.setNext(newNode); 
          count++;
        }
    }

    public void removeNext(Node<T> current) {
        Node<T> next1 = current.getNext();
        Node<T> next2 = null;
        if (next1 != null) {
            next2 = next1.getNext();
            current.setNext(next2);
        }
    }
    
    public void replace(Node<T> newNode, Node<T> oldNode) {
        oldNode.setData(newNode.getData());
        
    }

public Node<T> find(T key) {

        Node<T> current = first;
        while (current != null) {
            if ( current.getData().compareTo(key) == 0) {
                    return current;
            }
            current = current.getNext();
   }
return null;
    }

    public void showList() {
        System.out.println("\nFirst: \"" + first.getData() + "\" at  " + first);
        System.out.println("\nLast: \"" + last.getData() + "\" at " + last);
        System.out.println("\nCount: " + count);

        Node<T> current = first;
        while (current != null) {
            current.showNode();
            current = current.getNext();
        }
    }
}

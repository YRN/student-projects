
/*
 * Group 5
 * 
 * Objective: Uses data entered in gui to create a serialized object. 
 *                     Save and retrieve the entry as text or object file.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class SaveRetrieve {
	
	static String objFile = "data.bin";
	static String txtFile = "data.txt";
	
	// save as serialized object to data.bin
	public static void SaveObj(ArrayList<NameAddressID> objList) {

		ObjectOutputStream outObj;

		try {
			outObj = new ObjectOutputStream(new FileOutputStream(objFile));
			for (int i = 0; i < objList.size(); i++) {
				outObj.writeObject(objList.get(i));
			}
			outObj.flush();
			outObj.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	// prints entries from data.bin
	public static void ReadObj(ArrayList<NameAddressID> objList) {

		try {
			ObjectInputStream inObj = new ObjectInputStream(new FileInputStream(objFile));

			for (int i = 0; i < objList.size(); i++) {
				NameAddressID entry = (NameAddressID) inObj.readObject();
				System.out.println(entry.getFullNameSpaces());
				System.out.println(entry.getStreet());
				System.out.println(entry.getCity() + ", " + entry.getState() + " " + entry.getZip());
				System.out.println("ID: " + entry.getID());
				System.out.println();
			}
			inObj.close();

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	
	// save as formatted text file (data.txt)
	public static void saveTxt(NameAddressID entry) {

		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(txtFile, true));
			writer.write(entry.getFullNameSpaces());
			writer.newLine();
			writer.write(entry.getStreet());
			writer.newLine();
			writer.write(entry.getCity().toString());
			writer.write(",");
			writer.write(entry.getState().toString());
			writer.write(" ");
			writer.write(String.valueOf(entry.getZip()));
			writer.newLine();
			writer.write("ID: ");
			writer.write(String.valueOf(entry.getID()));
			writer.newLine();
			writer.write(" ");
			writer.newLine();
			 writer.flush();
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	// prints formatted entries from data.txt
	public static void readTxt() {

		String line = null;

		try {
			FileReader fileReader = new FileReader(txtFile);

			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while ((line = bufferedReader.readLine()) != null) {
				System.out.println(line);
			}

			bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	//cleans up files for testing purposes
	public static  void deleteFiles(){
		File obj= new File(objFile);
		File txt = new File(txtFile);
		
		obj.delete();
		txt.delete();
	}
	
}

/*
 * Group 5 assignment 3
 * 
 * Objective: GUI for entering data into NameAddressID
 */

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class NameAddressID_GUI extends Application{
	
	@Override
	public void start(Stage stage) {

		GridPane  inner = new GridPane();
		Pane commands = new Pane();
		Pane output = new Pane();
		BorderPane outer = new BorderPane();
		StackPane heading = new StackPane();

		outer.setTop(heading);
		outer.setCenter(inner);
		outer.setBottom(commands);
		outer.setRight(output); //TODO if possible, this is the pane I'd like to use to show read data
		
		//heading layout TODO change text to apply to purpose of future assignments
		heading.setMinSize(350, 75);
		heading.setStyle("-fx-background-color: yellowgreen;" +  "-fx-border-color: grey;" +
				"-fx-border-width: 2;");
		Text headingText = new Text("Group 5: Assignment 4");
		headingText.setFont(new Font("Cambria", 30));
		headingText.setFill(Color.WHITE);
		heading.getChildren().add(headingText);
		

		//inner layout
		inner.setAlignment(Pos.TOP_CENTER);
		inner.setPadding(new Insets(20, 0, 0 ,0));
		inner.setHgap(5);
		inner.setVgap(5);
		
		String[] labels = {"First Name:", "Middle Name: ", "Last Name: ", "Street: ", "City: ", "State: ", "Zip: ", "ID Number: "};
		for (int i = 0; i < labels.length; i++) {
			Label s = new Label(labels[i]);
			s.setStyle("-fx-Font: 15 Cambria;" + "-fx-Font-Weight: bold;");
			inner.add(s, 0, i);
		}

		TextField Fname = new TextField();
		TextField Mname = new TextField();
		TextField Lname = new TextField();
		TextField Street = new TextField();
		TextField City = new TextField();
		TextField State = new TextField();
		TextField Zip = new TextField();
		TextField ID = new TextField();		
		inner.addColumn(1, Fname, Mname, Lname, Street, City, State, Zip, ID);
		
		//command layout 
		Button saveObjBtn = new Button("SAVE Object");
		saveObjBtn.setStyle("-fx-Font-Weight: bold;");
		saveObjBtn.setLayoutX(40);
		saveObjBtn.setLayoutY(25);
		Button saveTxtBtn = new Button("SAVE Text File");
		saveTxtBtn.setStyle("-fx-Font-Weight: bold;");
		saveTxtBtn.setLayoutX(190);
		saveTxtBtn.setLayoutY(25);
		Button readObjBtn = new Button("READ Object");
		readObjBtn.setStyle("-fx-Font-Weight: bold;");
		readObjBtn.setLayoutX(40);
		readObjBtn.setLayoutY(60);
		Button readTxtBtn = new Button("READ Text File");
		readTxtBtn.setStyle("-fx-Font-Weight: bold;");
		readTxtBtn.setLayoutX(190);
		readTxtBtn.setLayoutY(60);
		commands.getChildren().addAll(saveObjBtn, saveTxtBtn, readObjBtn, readTxtBtn);
		commands.setMinSize(350, 100);			

		//show
		Scene scene = new Scene(outer);
		stage.setTitle("NameAddressID");
		stage.setScene(scene);
		stage.show();
	}

}

import java.util.ArrayList;

public class NameAddressID_Event {

	// event handler

	// Test method, change as needed
	// To use: comment out line 5 and uncomment line 7 in Driver.java
	public static void testObj() {
		
		ArrayList<NameAddressID> objList = new ArrayList<NameAddressID>();

		String fieldEntryExample = "Bob";
		String emptyFieldExample = "unknown";

		// make an entry
		String street = "34 Blackberry";
		SingleName city = new SingleName(new AlphaString(emptyFieldExample));
		SingleName state = new SingleName(new AlphaString("Ohio"));
		int zip = 44691;
		SingleName first = new SingleName(new AlphaString(fieldEntryExample));
		SingleName middle = new SingleName(new AlphaString("T."));
		SingleName last = new SingleName(new AlphaString("Smith"));
		int id = 555670;
		NameAddressID entry = new NameAddressID(street, city, state, zip, first, middle, last, id);

		// make a second entry
		String street2 = "697 Madison";
		SingleName city2 = new SingleName(new AlphaString("Lakewood"));
		SingleName state2 = new SingleName(new AlphaString("Ohio"));
		int zip2 = 44107;
		SingleName first2 = new SingleName(new AlphaString("Jane"));
		SingleName middle2 = new SingleName(new AlphaString("L."));
		SingleName last2 = new SingleName(new AlphaString("Doe"));
		int id2 = 66445;
		NameAddressID entry2 = new NameAddressID(street2, city2, state2, zip2, first2, middle2, last2, id2);
		
		objList.add(entry);
		SaveRetrieve.SaveObj(objList);
		objList.add(entry2);
		SaveRetrieve.SaveObj(objList);

		System.out.println();
		System.out.println("OBJECT FILE");
		System.out.println("-----------");
		SaveRetrieve.ReadObj(objList);
		
		// cleanup (things get really annoying to test without this)
		SaveRetrieve.deleteFiles();
	}

	public static void testTxt() {

		String fieldEntryExample = "Bob";
		String emptyFieldExample = "unknown";

		// make an entry
		String street = "34 Blackberry";
		SingleName city = new SingleName(new AlphaString(emptyFieldExample));
		SingleName state = new SingleName(new AlphaString("Ohio"));
		int zip = 44691;
		SingleName first = new SingleName(new AlphaString(fieldEntryExample));
		SingleName middle = new SingleName(new AlphaString("T."));
		SingleName last = new SingleName(new AlphaString("Smith"));
		int id = 555670;
		NameAddressID entry = new NameAddressID(street, city, state, zip, first, middle, last, id);

		// make a second entry
		String street2 = "697 Madison";
		SingleName city2 = new SingleName(new AlphaString("Lakewood"));
		SingleName state2 = new SingleName(new AlphaString("Ohio"));
		int zip2 = 44107;
		SingleName first2 = new SingleName(new AlphaString("Jane"));
		SingleName middle2 = new SingleName(new AlphaString("L."));
		SingleName last2 = new SingleName(new AlphaString("Doe"));
		int id2 = 66445;
		NameAddressID entry2 = new NameAddressID(street2, city2, state2, zip2, first2, middle2, last2, id2);

		// add the object to the array list and save as an object and a text file
		SaveRetrieve.saveTxt(entry);
		SaveRetrieve.saveTxt(entry2);

		// display text file and object
		System.out.println();
		System.out.println("TEXT FILE: ");
		System.out.println("-----------");
		SaveRetrieve.readTxt();

		// cleanup (things get really annoying to test without this)
		SaveRetrieve.deleteFiles();
	}

}

package csu.newman;

import java.util.Scanner;

// Author: Yvonne Newman
// Date: Mar 9, 2015
// Goal: Recursively move knight from 0,0 to user-input destination.
//--------------------------------------------------------------------------------------------------------

public class Driver {

    public static void main(String[] args) {

        //boardsize
        int n = 8;

        //chessboard array
        char[][] board = new char[n][n];

        //scanner for input
        Scanner input = new Scanner(System.in);

        //board setup
        for (int c = 0; c < board.length; c++) {
            for (int r = 0; r < board.length; r++) {
                board[c][r] = '_';
            }
        }

        //knight's starting position
        int kC = 0;
        int kR = 0;

        //ask user for input
        System.out.println("On an " + n + "x" + n + "chessboard, there is knight at column "
                + (kC + 1) + ", row " + (kR + 1) + ". Please enter the column and row you'd like him"
                + " to travel to.");
        System.out.println();

        //user input destination     
        System.out.println("Column: ");
        int dC = input.nextInt() - 1;
        System.out.println("Row: ");
        int dR = input.nextInt() - 1;

        //place pieces
        board[dC][dR] = 'X';
        board[kC][kR] = 'K';

        //print board setup
        printBoard(board);

        //knight checks and travels
        knightMoves(board, dR, dC, kR, kC, n, 0);

    }

    // print the current board
    private static void printBoard(char[][] board) {

        System.out.println();
        System.out.print("   1  2  3  4  5  6  7  8 \n");
        for (int r = 0; r < 8; r++) {
            System.out.print((r + 1) + "  ");
            for (int c = 0; c < 8; c++) {
                System.out.print(board[c][r] + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }

    // knight checks and travels
    private static void knightMoves(char[][] board, int dR, int dC, int kR, int kC, int n, int count) {

        //path to reach all spaces on the board, strting at 0,0.
        int cMoves[] = {1, 1, 1, 2, 2, -1, 1, -1, -2, 1, 1, 1, -2, 2, -1, 1, -2, 1, 1, -2, 1, -2,
            1, 1, -2, -1, 2, 2, -1, -2, -2, 2, -1, -2, -1, 2, -2, 2, 1, 1, -2, -2, 1, 2, -1, -1, -1, 1,
            2, -2, -1, 2, 2, -1, -1, -2, 1, -1, 1, 2, 1, 1, 2};

        int rMoves[] = {2, 2, 2, 1, -1, -2, -2, -2, 1, 2, 2, 2, -1, -1, -2, -2, -1, 2, 2, 1, 2, -1,
            -2, 2, -1, -2, -1, 1, -2, -1, 1, 1, -2, 1, 2, -1, -1, -1, 2, 2, -1, 1, 2, 1, -2, 2, -2, -2,
            1, 1, 2, -1, 1, -2, 2, -1, -2, -2, -2, 1, 2, -2, -1};

        //                  map:
        //                    0  59  38  33  30  17    8  63
        //                  37  34  31  60    9  62  29  16
        //                  58    1  36  39  32  27  18    7
        //                  35  48  41  26  61  10  15  28
        //                  42  57    2  49  40  23    6  19
        //                  47  50  45  54  25  20  11  14
        //                  56  43  52    3   22  13  24   5
        //                  51  46  55  44  53    4   21  12
        
        
        //check if the knight is at destination
        if (test(board, dR, dC, kR, kC)) {
            System.out.println("The knight has reached his destination");
            
        // check if near destination
        } else if (search(board, kR, kC, n)) {
            System.out.println("The knight moves to column " + (dC + 1) + ", row " + (dR + 1) + ".");
            
            board[dC][dR] = 'K';
            board[kC][kR] = '_';
            
            //print final board
            printBoard(board);
            System.out.println("The knight has reached his destination");

        //move the knight a step
        } else {
            move(board, dR, dC, kR, kC, cMoves, rMoves, n, count);
        }
    }

    //check if knight has reached destination
    private static boolean test(char[][] board, int dR, int dC, int kR, int kC) {

        if (board[kC][kR] == 'X' || board[dC][dR] == 'K') {
            return true;
        } else {
            return false;
        }
    }

    // check surrounding moves for the destination
    private static boolean search(char[][] board, int kR, int kC, int n) {

        if ((onBoard(kC + 2, kR + 1, n)) && (board[kC + 2][kR + 1] == 'X')) {
                return true;
                
            } else if ((onBoard(kC + 2, kR - 1, n)) && (board[kC + 2][kR - 1] == 'X')) {
                return true;
                
            } else if ((onBoard(kC - 2, kR + 1, n)) &&(board[kC + 2][kR + 1] == 'X')) {
                return true;
                
            } else if ((onBoard(kC - 2, kR - 1, n)) && (board[kC - 2][kR - 1] == 'X')) {
                return true;
                
            } else if ((onBoard(kC + 1, kR + 2, n)) && (board[kC + 1][kR + 2] == 'X')) {
                return true;
                
            } else if ((onBoard(kC + 1, kR - 2, n)) && (board[kC + 1][kR - 2] == 'X')) {
                return true;
                
            } else if ((onBoard(kC - 1, kR + 2, n)) && (board[kC - 1][kR + 2] == 'X')) {
                return true;
                
            } else if ((onBoard(kC - 1, kR - 2, n)) && (board[kC - 1][kR - 2] == 'X')) {
                return true;
                
            } else {        
                return false;
            }
    }

    // move the knight one step
    private static void move(char[][] board, int dR, int dC, int kR, int kC,
            int[] cMoves, int[] rMoves, int n, int count) {

        // check if valid move
        if (onBoard(kC + cMoves[count], kR + rMoves[count], n)) {

            board[kC][kR] = '_';
            kC += cMoves[count];
            kR += rMoves[count];
            board[kC][kR] = 'K';
            System.out.println("The knight moves to column " + (kC + 1)
                    + ", row " + (kR + 1));
            count += 1;

            // knight takes another step
            knightMoves(board, dR, dC, kR, kC, n, count);

         // the path isn't working
        } else {
            printBoard(board);
            System.out.println("Something has gone wrong.");
        }
    }

// check if move is on the 8x8 board
    public static boolean onBoard(int kC, int kR, int n) {

        if (!(kC >= 0 && kC < n && kR >= 0 && kR < n)) {
            return false;
        } else {
            return true;
        }
    }
}

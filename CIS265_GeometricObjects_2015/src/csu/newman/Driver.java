package csu.newman;

// Author: Yvonne Newman
// Date: Jan 29, 2015
// Goal: Implement GeometricObject class and test
//          ideas on inheritance through the extended 
//          classes CircleFromGO and RectangleFromGO.
//------------------------------------------------------------------

public class Driver {

    public static void main(String[] args) {

        Circle c1 = new Circle("Red", true, 2);
        System.out.println(c1);
        System.out.println();

        Rectangle r1 = new Rectangle();
        System.out.println(r1);
        System.out.println();
    }
}

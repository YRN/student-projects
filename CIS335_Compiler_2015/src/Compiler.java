import java.util.Scanner;

public class Compiler {

	static int REGA = 0;
	static int i, q, idx, index;
	static int t = 1;
	static String eqs, idi;
	static String[] s = new String[64];
	static String[] resw = new String[64];
	static char token, next, op;

	public static void main(String[] args) {
		s[0] = "RA";
		int p;
		String str;
		
        System.out.println("Enter equations, terminated with \";\". Enter \"quit\" to quit.");
        Scanner input = new Scanner(System.in);
        int counter = 0;
        while (true) {
            str = input.nextLine();
            if (str.equals("quit")) {
				System.out.println();
                break;
            } else {
                if (counter == 0)
                    eqs = str;
                else
                    eqs = eqs + str;
            }
            counter++;
          }
		
		System.out.println("---------- Grammar Rules ----------\n");

		System.out.println("<stmt-list> ::= <stmt> { <stmt> }");
		System.out.println("<stmt>        ::= id = <expr>");
		System.out.println("<expr>         ::= <term> { + <term> | - <term> }");
		System.out.println("<term>        ::= <factor> { * <factor> | / <factor> }");
		System.out.println("<factor>      ::=  id | intnum | ( <expr> )");

		System.out.println("\n---------- Equations From User Input ----------\n");

		for (p = 0; p < eqs.length(); p++) {

			if (eqs.charAt(p) == ';') {
				System.out.print(eqs.charAt(p));
				System.out.println();
			} else {
				System.out.print(eqs.charAt(p));
			}
		}

		System.out.println("\n---------- Corresponding Modified SIC/XE Output ----------\n");

		eqs = eqs.replaceAll("\\s+", "");
		lexme();
		assign();
				
		System.out.println();
		for (p = 0; p < q; p++) {
			System.out.println(resw[p]);
		}
	} // end main
	
	
	public static void lexme() {
		int len = eqs.length();
		if (index == len - 1) {
			token = eqs.charAt(index);
			next = ' ';
			return;
		} else {
			token = eqs.charAt(index);
			index++;
			if (index < len) {
				next = eqs.charAt(index);
			}
		}
	}

	public static void assign() {
		idx = 0;
		i = 0;
		op = 0;
		if (Character.isLetter(token)) {
			idi = String.valueOf(token);
			s[++idx] = idi;
			resw[q++] = idi + "     RESW     1";
			lexme();
			if (token == '=') {
				lexme();
				i = expr();

			}
			if (token == ';') {
				GETA(i);
				System.out.printf("      MOV      %%EAX,%s\n", idi);
				if (index < eqs.length()) {
					lexme();
					assign();
				}
			}
		}
	}

	public static int expr() {
		int j;
		i = term();
		while (token == '+' || token == '-') {
			op = token;
			lexme();
			j = factor();
			if (op == '+') {

				if (s[i].equals("RA")) {
					System.out.printf("      ADD      %s\n", s[j]);
				} else if (s[j].equals("RA")) {
					System.out.printf("      ADD      %s\n", s[i]);
				} else {
					GETA(i);
					System.out.printf("      ADD      %s\n", s[j]);
				}
				s[i] = "RA";
				REGA = i;
			} else {
				if (s[i].equals("RA")) {
					System.out.println("here");
					System.out.printf("      SUB      %s\n", s[j]);
				} else {
					GETA(i);
					System.out.printf("      SUB      %s\n", s[j]);
				}
				s[i] = "RA";
				REGA = i;
			}
		}
		return i;
	}

	public static int term() {
		int j;
		i = factor();
		while (token == '*' || token == '/') {
			op = token;
			lexme();
			j = factor();
			if (op == '*') {
				if (s[i] == "RA") {
					System.out.printf("      MUL      %s\n", s[j]);
				} else if (s[j] == "RA") {
					System.out.printf("      MUL      %s\n", s[i]);
				} else {
					GETA(i);
					System.out.printf("      MUL      %s\n", s[j]);
				}
				s[i] = "RA";
				REGA = i;
			} else {
				if (s[i] == "RA") {
					System.out.printf("      DIV      %s\n", s[j]);
				} else {
					GETA(i);
					System.out.printf("      DIV      %s\n", s[j]);
				}
				s[i] = "RA";
				REGA = idx;
			}
		}
		return i;
	}

	public static int factor() {

		if (Character.isLetter(token)) {
			s[++idx] = String.valueOf(token);
			lexme();
			return idx;
		} else if (Character.isDigit(token)) {
			s[++idx] = ("#" + token);
			lexme();
			return idx;
		} else if (token == '(') {
			lexme();
			expr();
			if (s[i] == "RA") {
				GETA(i);
			} else {
				System.out.println("ERROR");
			}
		}
		return i;
	}

	public static void GETA(int node) {
		String tp;
		if (REGA == 0) {
			System.out.printf("      MOV      %s,%%EAX\n", String.valueOf(s[node]));
		} else if (s[node] != "RA") {
			// create temp variable Ti
			tp = 'T' + String.valueOf(t);
			String newRes = idi + "     RESW     1";
			boolean isUnique = true;
			for (int iterator = 0; iterator < resw.length; iterator++) {
				if (newRes.equals(resw[iterator])){
					isUnique = false;
					break;
				}
			}
			if (isUnique) {
				resw[q++] = idi + "     RESW     1";
			}
			t++;
			System.out.printf("      MOV      %%EAX,%s\n", tp);
			s[REGA] = tp;
			lexme();
			System.out.print("      MOV      " + s[node] +",%EAX\n"); 
			s[node] = "RA";
			REGA = node;
		}
	}
}